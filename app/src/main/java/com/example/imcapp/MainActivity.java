package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Apenas usei a sugestão do Android Studio para que o problema com a variável fosse
        //resolvido
        final float[] IMC = new float[1];

        Button btCalculo = (Button) findViewById(R.id.btCalculo);

        final ImageView imageView = (ImageView) findViewById(R.id.imageView);

        btCalculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView peso = (TextView) findViewById(R.id.peso);
                TextView altura = (TextView) findViewById(R.id.altura);
                TextView Resultado = (TextView) findViewById(R.id.Resultado);
                TextView Faixa = (TextView) findViewById(R.id.Faixa);

                int cxPeso = Integer.parseInt(peso.getText().toString());
                float cxAltura = Float.parseFloat(altura.getText().toString());

                IMC[0] = cxPeso / (cxAltura * cxAltura);

                if (IMC[0] < 18.5) {
                    Resultado.setText(IMC[0] + "");
                    Faixa.setText("Abaixo do peso");
                    imageView.setImageResource(R.drawable.abaixopeso);
                } else {
                    if (IMC[0] >= 18.5 || IMC[0] < 25) {
                        Resultado.setText(IMC[0] + "");
                        Faixa.setText("Peso de acordo com a normalidade");
                        imageView.setImageResource(R.drawable.normal);
                    } else {
                        if (IMC[0] >= 25 || IMC[0] < 30) {
                            Resultado.setText(IMC[0] + "");
                            Faixa.setText("Acima do peso");
                            imageView.setImageResource(R.drawable.obesidade1);
                        } else {
                            if (IMC[0] >= 25 || IMC[0] < 30) {
                                Resultado.setText(IMC[0] + "");
                                Faixa.setText("Muito cima do peso");
                                imageView.setImageResource(R.drawable.obesidade2);
                            } else {
                                Resultado.setText(IMC[0] + "");
                                Faixa.setText("Obesidade");
                                imageView.setImageResource(R.drawable.obesidade3);
                            }
                        }
                    }
                }
            }
        });
    }
}